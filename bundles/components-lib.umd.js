(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core')) :
    typeof define === 'function' && define.amd ? define('components-lib', ['exports', '@angular/core'], factory) :
    (global = global || self, factory(global['components-lib'] = {}, global.ng.core));
}(this, (function (exports, core) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/components-lib.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ComponentsLibComponent = /** @class */ (function () {
        function ComponentsLibComponent() {
        }
        /**
         * @return {?}
         */
        ComponentsLibComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
        };
        ComponentsLibComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'comp-components-lib',
                        template: "\n    <p>\n      components-lib works!\n    </p>\n  "
                    }] }
        ];
        /** @nocollapse */
        ComponentsLibComponent.ctorParameters = function () { return []; };
        return ComponentsLibComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/components-lib.module.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ComponentsLibModule = /** @class */ (function () {
        function ComponentsLibModule() {
        }
        ComponentsLibModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [ComponentsLibComponent],
                        imports: [],
                        exports: [ComponentsLibComponent]
                    },] }
        ];
        return ComponentsLibModule;
    }());

    exports.ComponentsLibModule = ComponentsLibModule;
    exports.ɵa = ComponentsLibComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=components-lib.umd.js.map
